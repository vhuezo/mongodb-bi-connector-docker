# Mongodb BI Connector

Imagen Docker de  mongodb-bi-connector.

## Security / Auth

Este Mongodb BI Connector se asume que su servidor mongodb puede conectarse sin ninguna autenticación (red confiable).
"*La autenticación en MongoDB es bastante compleja*". Esa es la razón por la que hacer que este Mongodb BI Connector sea lo más simple posible por ahora.

## docker-compose example

```
version: "3"
services:
  mongodb:
    image: mongo:bionic
  
  mongodb-bi-connector:
    image: registry.gitlab.com/vhuezo/mongodb-bi-connector-docker:latest
    environment:
      MONGODB_HOST: mongodb
      MONGODB_PORT: 27017  

```

#docker-compose

```
version: "3"
services:
  mongodb:
    image: mongo:bionic
    restart: always
    ports:
      - "27017:27017"
    volumes:
      - ./mongo/mongo-volumen:/data/db
      - ./mongo/mongo-config-volumen:/data/configdb
  mongodb-bi-connector:
    image: registry.gitlab.com/vhuezo/mongodb-bi-connector-docker:latest
    restart: always
    ports:
      - "3307:3307"
    environment:
      MONGODB_HOST: mongodb
      MONGODB_PORT: 27017  
      
```


```
